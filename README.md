# **MI-MVI Report**<br/>Impact of Fast Fourier Transformation introduced in different layers of Convolutional Neural Network
### Richard Werner<br/>FIT CTU in Prague<br/>January 2020

## Introduction
This project is based on a blogpost [Fun with Fourier](https://towardsdatascience.com/fun-with-fourier-591662576a77).

It experiments with introducing a Fourier transformation into different parts of a convolutional neural network and then testing it's performance.
The network is classifying raw audio waves into 10 environmental sound classes (dog, chainsaw, helicopter, rain, etc.) using the ESC-10 subset of [ESC-50 dataset](https://github.com/karolpiczak/ESC-50).

The goal was to replicate the experiments from the blogpost on a dataset, which I've been lately working with - the [Czech Parliament Meetings dataset](https://lindat.mff.cuni.cz/repository/xmlui/handle/11858/00-097C-0000-0005-CF9C-4?fbclid=IwAR3KCJk-TtYHq6VtcjZlDdL_phswtDMtU_VeaCgyRfC-dHjvrYrsd1amrzg).

## Input data - CPM dataset

This dataset consists of several 4 to 6 hours long recordings in raw WAV audio format.
Each of them has their own transcription file in XML type format.
Apart from transcription, the files also contain information about mispronunciations, noises and mainly **current speakers**, which will later on serve as my calssification labels.

The desired data format for feeding the CNN is several seconds long audion chunks with information about the speaker.
Luckily, I've already cut the audio files into chunks with transcritpions for my other [speech2text](https://github.com/opendatalabcz/speech2text) project, so the only thing remaining is to assign corresponding speaker names to the individual audio chunks.
All the code used to preprocess the dataset is located in the speech2text repository, specifically in [this](https://github.com/opendatalabcz/speech2text/blob/master/speech2text/DatasetManipulator.py) file.
Since I've already had the csv files for train/dev/test data for the speech2text project and wasn't sure if I could mess with the columns in the csv files, I've created a new text file for each audio chunk with it's speaker name.
This way nothing of my previous work is changed and I've got everything I need for this project.

Example of the prepared data is in [this](https://gitlab.fit.cvut.cz/werneric/mvi-sp/tree/master/data_sample) directory.
There are three files for each entry: the audio file (.wav), the speaker text file (.spk) and the transcription (.txt, which is not used in this project).

## Data preprocessing before feeding the CNN

There are a few steps necessary before I can feed the audio to the neural network.
Those are:
* Removing entries with empty speaker information
    * That's what the algorithm falls to when it encounters a problem - creates an empty file instead of crashing or creating a false label
* Selecting only those entries where speaker is in the top N frequent speakers
    * It's for the purpose of having enough data for each of the classes
    * N equals 10 in this case - same as the ESC-10 dataset
* Cutting the sound waves to the same length
    * That consists of two parts:
        1. Selecting entries with length of at least some constant minimum (100 000 audio samples/frames in this case)
        2. Cutting the audio
    * I'm always cutting (extracting) the middle part of the wave (which means equally trimming it's sides), since the sides are more likely to be silent without anyone speaking
* Creation of encoder/decoder for the speaker label
    * As the NN expects integral or binary vector targets, I had to encode the label to integers using dictionaries
* Data oversampling
    * To ensure class equality, I've used random oversampling which simply randomly copies data of minority classes to be all equally large
* Inegral labels to binary matrices
    * Lastly, the labels need to be again converted, this time to binary matrices (vectors) using the tf.keras.utils.to_categorical() function
    * It needs to be done for the sake of categorical_crossentropy loss function

## Neural Network

For the neural net I've used the same approach as in the blogpost - 1D convolutional net with 2 conv. layers, 
max pooling, another 2 conv. layer, global average pooling and dropout layer.
Everything done in tensorflow's Keras framework.
The basic model without the FFT is shown below:

```python
model_A = tf.keras.Sequential()
model_A.add(Reshape((100000, 1), input_shape=(100000,)))
model_A.add(Conv1D(100, 10, activation='relu'))
model_A.add(Conv1D(100, 10, activation='relu'))
model_A.add(MaxPooling1D(10))
model_A.add(Conv1D(160, 10, activation='relu'))
model_A.add(Conv1D(160, 10, activation='relu'))
model_A.add(GlobalAveragePooling1D())
model_A.add(Dropout(0.5))
model_A.add(Dense(10, activation='softmax'))
```

Other two models and all the remaining code is in the jupyter notebook [here](https://gitlab.fit.cvut.cz/werneric/mvi-sp/blob/master/SoundCNN.ipynb).

## Experiment results
As for the results - they kind of differ from the blogpost results.
On the picture below, there's accuracy performance over 40 epochs on the CPM dataset.

<img src="./img/speaker.png" alt="CNN performance on the CPM dataset" width="600">

We can see that introduction of the FFT on the input data had almost no impact on the learning curve.
Moreover I am not even able to determine whether it had marginaly positive or negative effect, where in the original experiment, this had slightly positive effect.
On the other hand, introducing the FFT layer into intermediate layer had quite a big impact, yet negative.
This is in contradiction with the blogpost, where this had highly positive impact.

I've also tried to replicate the experiments on the same ESC-10 dataset and... was disappointed again. Have a look:

<img src="./img/ESC-10.png" alt="CNN performance on the ESC-10" width="600">

FFT in the intermediate layers had again pretty poor performance.
On the other hand, I'd be at least more confident to say that the input FFT had slightly positive impact on the learning curve.

The latter experiment led me to conclusion, that I had to have a different approach or 
used somewhat different (version of) technology for the neural network layers since the data we used was exactly the same.
Except maybe different train/test split or factor of randomness, neither of which is the reason our results are that different.

## Summary

To sum it up, the expertiment didn't go as expected.
Although I've tried to replicate the experiment as much as possible, our results differ by a lot.
Whether there's a mistake in my experiment (which doesn't seem likely since the training and validation goes as it should) 
or there's a difference in used technology is hard to say.

As for the future work or this project extension, it would be interesting to expertiment with more difficult data.
For example more classes from the CPM dataset or different dataset with less audible or noisy speech or environmental sounds.
Maybe for this kind of data, the FFT would have more significant (positive) impact.